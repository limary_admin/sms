<?php

namespace Sinta\Sms;

use Closure;
use RuntimeException;

use Sinta\Sms\Contracts\GatewayInterface;
use Sinta\Sms\Contracts\StrategyInterface;
use Sinta\Sms\Exceptions\InvalidArgumentException;
use Sinta\Sms\Strategies\OrderStrategy;
use Sinta\Sms\Support\Config;

class Sms
{
    protected $config;


    protected $defaultGateway;


    protected $customCreators = [];


    protected $gateways = [];


    protected $messenger;


    protected $strategies = [];


    public function __construct(array $config)
    {
        $this->config = new Config($config);
        if (!empty($config['default'])) {
            $this->setDefaultGateway($config['default']);
        }
    }

    /**
     * 发送消息
     *
     * @param $to
     * @param $message
     * @param array $gateways
     * @return array
     */
    public function send($to, $message, array $gateways = [])
    {
        return $this->getMessenger()->send($to, $message, $gateways);
    }


    public function gateway($name = null)
    {
        $name = $name ?: $this->getDefaultGateway();
        if (!isset($this->gateways[$name])) {
            $this->gateways[$name] = $this->createGateway($name);
        }
        return $this->gateways[$name];
    }

    public function strategy($strategy = null)
    {
        if (is_null($strategy)) {
            $strategy = $this->config->get('default.strategy', OrderStrategy::class);
        }
        if (!class_exists($strategy)) {
            $strategy = __NAMESPACE__.'\Strategies\\'.ucfirst($strategy);
        }
        if (!class_exists($strategy)) {
            throw new InvalidArgumentException("Unsupported strategy \"{$strategy}\"");
        }
        if (empty($this->strategies[$strategy]) || !($this->strategies[$strategy] instanceof StrategyInterface)) {
            $this->strategies[$strategy] = new $strategy($this);
        }
        return $this->strategies[$strategy];
    }


    public function extend($name, Closure $callback)
    {
        $this->customCreators[$name] = $callback;
        return $this;
    }


    public function getConfig()
    {
        return $this->config;
    }

    public function getDefaultGateway()
    {
        if (empty($this->defaultGateway)) {
            throw new RuntimeException('No default gateway configured.');
        }
        return $this->defaultGateway;
    }


    public function setDefaultGateway($name)
    {
        $this->defaultGateway = $name;
        return $this;
    }


    public function getMessenger()
    {
        return $this->messenger ?: $this->messenger = new Messenger($this);
    }

    protected function createGateway($name)
    {
        if (isset($this->customCreators[$name])) {
            $gateway = $this->callCustomCreator($name);
        } else {
            $className = $this->formatGatewayClassName($name);
            $gateway = $this->makeGateway($className, $this->config->get("gateways.{$name}", []));
        }
        if (!($gateway instanceof GatewayInterface)) {
            throw new InvalidArgumentException(sprintf('Gateway "%s" not inherited from %s.', $name, GatewayInterface::class));
        }
        return $gateway;
    }

    protected function makeGateway($gateway, $config)
    {
        if (!class_exists($gateway)) {
            throw new InvalidArgumentException(sprintf('Gateway "%s" not exists.', $gateway));
        }
        return new $gateway($config);
    }

    protected function formatGatewayClassName($name)
    {
        if (class_exists($name)) {
            return $name;
        }
        $name = ucfirst(str_replace(['-', '_', ''], '', $name));
        return __NAMESPACE__."\\Gateways\\{$name}Gateway";
    }


    protected function callCustomCreator($gateway)
    {
        return call_user_func($this->customCreators[$gateway], $this->config->get($gateway, []));
    }


}