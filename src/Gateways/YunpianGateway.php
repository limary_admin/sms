<?php

namespace Sinta\Sms\Gateways;

use Sinta\Sms\Contracts\MessageInterface;
use Sinta\Sms\Exceptions\GatewayErrorException;
use Sinta\Sms\Support\Config;
use Sinta\Sms\Traits\HasHttpRequest;

/**
 *
 * 云片
 *
 * Class YunpianGateway
 * @package Sinta\Sms\Gateways
 * @see https://www.yunpian.com/api2.0/api-domestic/single_send.html
 */
class YunpianGateway extends Gateway
{
    use HasHttpRequest;

    const ENDPOINT_TEMPLATE = 'https://%s.yunpian.com/%s/%s/%s.%s';
    const ENDPOINT_VERSION = 'v2';
    const ENDPOINT_FORMAT = 'json';

    public function getName()
    {
        return "yunpian";
    }


    public function send($to, MessageInterface $message, Config $config)
    {
        $endpoint = $this->buildEndpoint('sms', 'sms', 'single_send');
        $result = $this->post($endpoint, [
            'apikey' => $config->get('api_key'),
            'mobile' => $to,
            'text' => $message->getContent($this),
        ]);
        if ($result['code']) {
            throw new GatewayErrorException($result['msg'], $result['code'], $result);
        }
        return $result;
    }


    protected function buildEndpoint($type, $resource, $function)
    {
        return sprintf(self::ENDPOINT_TEMPLATE, $type, self::ENDPOINT_VERSION, $resource, $function, self::ENDPOINT_FORMAT);
    }
}