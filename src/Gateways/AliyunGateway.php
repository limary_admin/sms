<?php

namespace Sinta\Sms\Gateways;


use Sinta\Sms\Contracts\MessageInterface;
use Sinta\Sms\Exceptions\GatewayErrorException;
use Sinta\Sms\Traits\HasHttpRequest;
use Sinta\Sms\Support\Config;

/**
 * 阿里云网关
 *
 * Class AliyunGateway
 * @package Sinta\Sms\Gateways
 */
class AliyunGateway extends Gateway
{
    use HasHttpRequest;


    const ENDPOINT_URL = 'http://dysmsapi.aliyuncs.com';
    const ENDPOINT_METHOD = 'SendSms';
    const ENDPOINT_VERSION = '2017-05-25';
    const ENDPOINT_FORMAT = 'JSON';
    const ENDPOINT_REGION_ID = 'cn-hangzhou';
    const ENDPOINT_SIGNATURE_METHOD = 'HMAC-SHA1';
    const ENDPOINT_SIGNATURE_VERSION = '1.0';


    public function getName()
    {
        return 'aliyun';
    }


    public function send($to, MessageInterface $message, Config $config)
    {
        $params = [
            'RegionId' => self::ENDPOINT_REGION_ID,
            'AccessKeyId' => $config->get('access_key_id'),
            'Format' => self::ENDPOINT_FORMAT,
            'SignatureMethod' => self::ENDPOINT_SIGNATURE_METHOD,
            'SignatureVersion' => self::ENDPOINT_SIGNATURE_VERSION,
            'SignatureNonce' => uniqid(),
            'Timestamp' => $this->getTimestamp(),
            'Action' => self::ENDPOINT_METHOD,
            'Version' => self::ENDPOINT_VERSION,
            'PhoneNumbers' => strval($to),
            'SignName' => $config->get('sign_name'),
            'TemplateCode' => $message->getTemplate($this),
            'TemplateParam' => json_encode($message->getData($this), JSON_FORCE_OBJECT),
        ];
        $params['Signature'] = $this->generateSign($params);
        $result = $this->get(self::ENDPOINT_URL, $params);
        if ('OK' != $result['Code']) {
            throw new GatewayErrorException($result['Message'], $result['Code'], $result);
        }
        return $result;
    }


    protected function generateSign($params)
    {
        ksort($params);
        $accessKeySecret = $this->config->get('access_key_secret');
        $stringToSign = 'GET&%2F&'.urlencode(http_build_query($params, null, '&', PHP_QUERY_RFC3986));
        return base64_encode(hash_hmac('sha1', $stringToSign, $accessKeySecret.'&', true));
    }


    protected function getTimestamp()
    {
        $timezone = date_default_timezone_get();
        date_default_timezone_set('GMT');
        $timestamp = date('Y-m-d\TH:i:s\Z');
        date_default_timezone_set($timezone);
        return $timestamp;
    }
}