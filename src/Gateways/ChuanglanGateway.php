<?php
namespace Sinta\Sms\Gateways;


use Sinta\Sms\Contracts\MessageInterface;
use Sinta\Sms\Exceptions\GatewayErrorException;
use Sinta\Sms\Support\Config;
use Sinta\Sms\Traits\HasHttpRequest;

class ChuanglanGateway extends Gateway
{
    use HasHttpRequest;

    const ENDPOINT_URL = 'https://sms.253.com/msg/send';

    public function getName()
    {
        return 'chuanglan';
    }

    public function send($to, MessageInterface $message, Config $config)
    {
        $params = [
            'un' => $config->get('username'),
            'pw' => $config->get('password'),
            'phone' => $to,
            'msg' => $message->getContent(),
        ];
        $result = $this->get(self::ENDPOINT_URL, $params);
        $formatResult = $this->formatResult($result);
        if (!empty($formatResult[1])) {
            throw new GatewayErrorException($result, $formatResult[1], $formatResult);
        }
        return $result;
    }

    protected function formatResult($result)
    {
        $result = str_replace("\n", ',', $result);
        return explode(',', $result);
    }


}