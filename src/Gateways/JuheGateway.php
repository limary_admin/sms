<?php

namespace Sinta\Sms\Gateways;


use Sinta\Sms\Contracts\MessageInterface;
use Sinta\Sms\Exceptions\GatewayErrorException;
use Sinta\Sms\Traits\HasHttpRequest;
use Sinta\Sms\Support\Config;

/**
 * 聚合网关
 *
 * Class JuheGateway
 * @package Sinta\Sms\Gateways
 *
 * @see https://www.juhe.cn/docs/api/id/54
 */
class JuheGateway extends Gateway
{
    use HasHttpRequest;


    const ENDPOINT_URL = 'http://v.juhe.cn/sms/send';
    const ENDPOINT_FORMAT = 'json';

    public function getName()
    {
        return 'juhe';
    }

    public function send($to, MessageInterface $message, Config $config)
    {
        $params = [
            'mobile' => $to,
            'tpl_id' => $message->getTemplate($this),
            'tpl_value' => $this->formatTemplateVars($message->getData($this)),
            'dtype' => self::ENDPOINT_FORMAT,
            'key' => $config->get('app_key'),
        ];
        $result = $this->get(self::ENDPOINT_URL, $params);
        if ($result['error_code']) {
            throw new GatewayErrorException($result['reason'], $result['error_code'], $result);
        }
        return $result;
    }


    protected function formatTemplateVars(array $vars)
    {
        $formatted = [];
        foreach ($vars as $key => $value) {
            $formatted[sprintf('#%s#', trim($key, '#'))] = $value;
        }
        return http_build_query($formatted);
    }

}