<?php

namespace Sinta\Sms\Gateways;


use Sinta\Sms\Contracts\MessageInterface;
use Sinta\Sms\Exceptions\GatewayErrorException;
use Sinta\Sms\Traits\HasHttpRequest;
use Sinta\Sms\Support\Config;

/**
 * 阿里达网关
 *
 * Class AlidayuGateway
 * @package Sinta\Sms\Gateways
 */
class AlidayuGateway extends Gateway
{
    use HasHttpRequest;


    const ENDPOINT_URL = 'https://eco.taobao.com/router/rest';
    const ENDPOINT_METHOD = 'alibaba.aliqin.fc.sms.num.send';
    const ENDPOINT_VERSION = '2.0';
    const ENDPOINT_FORMAT = 'json';

    public function getName()
    {
        return 'alidayu';
    }


    /**
     * 以送短信
     *
     * @param $to
     * @param MessageInterface $message
     * @param Config $config
     * @return mixed
     * @throws GatewayErrorException
     */
    public function send($to, MessageInterface $message, Config $config)
    {
        $params = [
            'method' => self::ENDPOINT_METHOD,
            'format' => self::ENDPOINT_FORMAT,
            'v' => self::ENDPOINT_VERSION,
            'sign_method' => 'md5',
            'timestamp' => date('Y-m-d H:i:s'),
            'sms_type' => 'normal',
            'sms_free_sign_name' => $config->get('sign_name'),
            'app_key' => $config->get('app_key'),
            'sms_template_code' => $message->getTemplate($this),
            'rec_num' => strval($to),
            'sms_param' => json_encode($message->getData($this)),
        ];
        $params['sign'] = $this->generateSign($params);
        $result = $this->post(self::ENDPOINT_URL, $params);
        if (!empty($result['error_response'])) {
            if (isset($result['error_response']['sub_msg'])) {
                $message = $result['error_response']['sub_msg'];
            } else {
                $message = $result['error_response']['msg'];
            }
            throw new GatewayErrorException($message, $result['error_response']['code'], $result);
        }
        return $result;
    }

    /**
     * 产生签名
     *
     * @param $params
     * @return string
     */
    protected function generateSign($params)
    {
        ksort($params);
        $stringToBeSigned = $this->config->get('app_secret');
        foreach ($params as $key => $value) {
            if (is_string($value) && '@' != substr($value, 0, 1)) {
                $stringToBeSigned .= "$key$value";
            }
        }
        $stringToBeSigned .= $this->config->get('app_secret');
        return strtoupper(md5($stringToBeSigned));
    }
}