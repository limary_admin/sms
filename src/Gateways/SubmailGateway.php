<?php

namespace Sinta\Sms\Gateways;

use Sinta\Sms\Contracts\MessageInterface;
use Sinta\Sms\Exceptions\GatewayErrorException;
use Sinta\Sms\HasHttpRequest;
use Sinta\Sms\Support\Config;

/**
 * 云通信
 *
 * Class SubmailGateway
 * @package Sinta\Sms\Gateways
 *
 * @see https://www.mysubmail.com/chs/documents/developer/index
 */
class SubmailGateway extends Gateway
{
    use HasHttpRequest;


    const ENDPOINT_TEMPLATE = 'https://api.mysubmail.com/message/%s.%s';
    const ENDPOINT_FORMAT = 'json';


    public function send($to, MessageInterface $message, Config $config)
    {
        $endpoint = $this->buildEndpoint('xsend');
        $result = $this->post($endpoint, [
            'appid' => $config->get('app_id'),
            'signature' => $config->get('app_key'),
            'project' => $config->get('project'),
            'to' => $to,
            'vars' => json_encode($message->getData($this), JSON_FORCE_OBJECT),
        ]);
        if ('success' != $result['status']) {
            throw new GatewayErrorException($result['msg'], $result['code'], $result);
        }
        return $result;
    }


    protected function buildEndpoint($function)
    {
        return sprintf(self::ENDPOINT_TEMPLATE, $function, self::ENDPOINT_FORMAT);
    }
}