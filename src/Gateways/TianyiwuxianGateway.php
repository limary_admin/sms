<?php
namespace Sinta\Sms\Gateways;


use Sinta\Sms\Contracts\MessageInterface;
use Sinta\Sms\Exceptions\GatewayErrorException;
use Sinta\Sms\Support\Config;

/**
 * 天毅无线
 *
 * Class TianyiwuxianGateway
 * @package Sinta\Sms\Gateways
 */
class TianyiwuxianGateway extends Gateway
{
    const ENDPOINT_TEMPLATE = 'http://jk.106api.cn/sms%s.aspx';
    const ENDPOINT_ENCODE = 'UTF8';
    const ENDPOINT_TYPE = 'send';
    const ENDPOINT_FORMAT = 'json';
    const SUCCESS_STATUS = 'success';
    const SUCCESS_CODE = '0';


    public function send($to, MessageInterface $message, Config $config)
    {
        $endpoint = $this->buildEndpoint();
        $params = [
            'gwid' => $config->get('gwid'),
            'type' => self::ENDPOINT_TYPE,
            'rece' => self::ENDPOINT_FORMAT,
            'mobile' => $to,
            'message' => $message->getContent(),
            'username' => $config->get('username'),
            'password' => strtoupper(md5($config->get('password'))),
        ];
        $result = $this->post($endpoint, $params);
        $result = json_decode($result, true);
        if (self::SUCCESS_STATUS !== $result['returnstatus'] || self::SUCCESS_CODE !== $result['code']) {
            throw new GatewayErrorException($result['remark'], $result['code']);
        }
        return $result;
    }

    public function getName()
    {
        return 'tainyiwuxian';
    }

    protected function buildEndpoint()
    {
        return sprintf(self::ENDPOINT_TEMPLATE, self::ENDPOINT_ENCODE);
    }
}