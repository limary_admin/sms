<?php

namespace Sinta\Sms\Gateways;


use Sinta\Sms\Contracts\MessageInterface;
use Sinta\Sms\Exceptions\GatewayErrorException;
use Sinta\Sms\Support\Config;
use Sinta\Sms\Traits\HasHttpRequest;


/**
 *
 *
 * Class LuosimaoGateway
 * @package Sinta\Sms\Gateways
 *
 * @see https://luosimao.com/docs/api/
 */
class LuosimaoGateway extends Gateway
{
    use HasHttpRequest;

    const ENDPOINT_TEMPLATE = 'https://%s.luosimao.com/%s/%s.%s';
    const ENDPOINT_VERSION = 'v1';
    const ENDPOINT_FORMAT = 'json';

    public function send($to, MessageInterface $message, Config $config)
    {
        $endpoint = $this->buildEndpoint('sms-api', 'send');
        $result = $this->post($endpoint, [
            'mobile' => $to,
            'message' => $message->getContent($this),
        ], [
            'Authorization' => 'Basic '.base64_encode('api:key-'.$config->get('api_key')),
        ]);
        if ($result['error']) {
            throw new GatewayErrorException($result['msg'], $result['error'], $result);
        }
        return $result;
    }


    protected function buildEndpoint($type, $function)
    {
        return sprintf(self::ENDPOINT_TEMPLATE, $type, self::ENDPOINT_VERSION, $function, self::ENDPOINT_FORMAT);
    }
}