<?php

namespace Sinta\Sms\Gateways;

use Sinta\Sms\Contracts\GatewayInterface;
use Sinta\Sms\Support\Config;

/**
 * 网关抽像类
 *
 * Class Gateway
 * @package Sinta\Sms\Gateways
 */
abstract class Gateway implements GatewayInterface
{
    const DEFAULT_TIMEOUT = 5.0;


    protected $config;


    protected $timeout;


    public function __construct(array $config)
    {
        $this->config = new Config($config);
    }


    public function getTimeout()
    {
        return $this->timeout ?: $this->config->get('timeout', self::DEFAULT_TIMEOUT);
    }


    public function setTimeout($timeout)
    {
        $this->timeout = floatval($timeout);
        return $this;
    }


    public function getConfig()
    {
        return $this->config;
    }


    public function setConfig(Config $config)
    {
        $this->config = $config;
        return $this;
    }

    public function getName()
    {
        return '';
    }

}