<?php

namespace Sinta\Sms\Gateways;


use Sinta\Sms\Contracts\MessageInterface;
use Sinta\Sms\Exceptions\GatewayErrorException;
use Sinta\Sms\Traits\HasHttpRequest;
use Sinta\Sms\Support\Config;

/**
 * Class HuyiGateway
 * @package Sinta\Sms\Gateways
 *
 * @see http://www.ihuyi.com/api/sms.html
 */
class HuyiGateway extends Gateway
{
    use HasHttpRequest;


    const ENDPOINT_URL = 'http://106.ihuyi.com/webservice/sms.php?method=Submit';
    const ENDPOINT_FORMAT = 'json';
    const SUCCESS_CODE = 2;


    public function getName()
    {
        return 'huyi';
    }


    public function send($to, MessageInterface $message, Config $config)
    {
        $params = [
            'account' => $config->get('api_id'),
            'mobile' => strval($to),
            'content' => $message->getContent(),
            'time' => time(),
            'format' => self::ENDPOINT_FORMAT,
        ];
        $params['password'] = $this->generateSign($params);
        $result = $this->post(self::ENDPOINT_URL, $params);
        if (self::SUCCESS_CODE != $result['code']) {
            throw new GatewayErrorException($result['msg'], $result['code'], $result);
        }
        return $result;
    }

    protected function generateSign($params)
    {
        return md5($params['account'].$this->config->get('api_key').$params['mobile'].$params['content'].$params['time']);
    }


}