<?php
namespace Sinta\Sms\Gateways;

use Sinta\Sms\Contracts\MessageInterface;
use Sinta\Sms\Exceptions\GatewayErrorException;
use Sinta\Sms\Traits\HasHttpRequest;
use Sinta\Sms\Support\Config;

/**
 * Class SendcloudGateway
 * @package Sinta\Sms\Gateways
 *
 * @see http://sendcloud.sohu.com/doc/sms/
 */
class SendcloudGateway extends Gateway
{
    use HasHttpRequest;

    const ENDPOINT_TEMPLATE = 'http://www.sendcloud.net/smsapi/%s';

    public function getName()
    {
        return 'sendcloud';
    }


    public function send($to, MessageInterface $message, Config $config)
    {
        $params = [
            'smsUser' => $config->get('sms_user'),
            'templateId' => $message->getTemplate($this),
            'phone' => is_array($to) ? implode(',', $to) : $to,
            'vars' => $this->formatTemplateVars($message->getData($this)),
        ];
        if ($config->get('timestamp', false)) {
            $params['timestamp'] = time() * 1000;
        }
        $params['signature'] = $this->sign($params, $config->get('sms_key'));
        $result = $this->post(sprintf(self::ENDPOINT_TEMPLATE, 'send'), $params);
        if (!$result['result']) {
            throw new GatewayErrorException($result['message'], $result['statusCode'], $result);
        }
        return $result;
    }


    protected function formatTemplateVars(array $vars)
    {
        $formatted = [];
        foreach ($vars as $key => $value) {
            $formatted[sprintf('%%%s%%', trim($key, '%'))] = $value;
        }
        return json_encode($formatted, JSON_FORCE_OBJECT);
    }


    protected function sign($params, $key)
    {
        ksort($params);
        return md5(sprintf('%s&%s&%s', $key, urldecode(http_build_query($params)), $key));
    }


}