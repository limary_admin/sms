<?php
namespace Sinta\Sms;

use Sinta\Sms\Support\Config;
use Sinta\Sms\Contracts\MessageInterface;
use Sinta\Sms\Exceptions\GatewayErrorException;
use Sinta\Sms\Exceptions\NoGatewayAvailableException;


/**
 * 信使
 *
 * Class Messenger
 * @package Sinta\Sms
 */
class Messenger
{

    const STATUS_SUCCESS = 'success';
    const STATUS_FAILED = 'failed';


    protected $sms;

    public function __construct(Sms $sms)
    {
        $this->sms = $sms;
    }

    /**
     * 发送短信
     *
     * @param $to
     * @param $message
     * @param array $gateways
     * @return array
     * @throws Exceptions\InvalidArgumentException
     * @throws NoGatewayAvailableException
     */
    public function send($to,$message, array $gateways = [])
    {
        $message = $this->formatMessage($message);

        if (empty($gateways)) {
            $gateways = $message->getGateways();
        }

        if (empty($gateways)) {
            $gateways = $this->sms->getConfig()->get('default.gateways', []);
        }

        $gateways = $this->formatGateways($gateways);
        $strategyAppliedGateways = $this->sms->strategy()->apply($gateways);

        $results = [];
        $hasSucceed = false;
        foreach ($strategyAppliedGateways as $gateway) {
            try {
                $results[$gateway] = [
                    'status' => self::STATUS_SUCCESS,
                    'result' => $this->sms->gateway($gateway)->send($to, $message, new Config($gateways[$gateway])),
                ];
                $hasSucceed = true;
                break;
            } catch (GatewayErrorException $e) {
                $results[$gateway] = [
                    'status' => self::STATUS_FAILED,
                    'exception' => $e,
                ];
                continue;
            }
        }

        if (!$hasSucceed) {
            throw new NoGatewayAvailableException($results);
        }
        return $results;
    }

    /**
     * 处理短信
     *
     * @param $message
     * @return array|Message
     */
    protected function formatMessage($message)
    {
        if (!($message instanceof MessageInterface)) {
            if (!is_array($message)) {
                $message = [
                    'content' => strval($message),
                    'template' => strval($message),
                ];
            }

            $message = new Message($message);
        }
        return $message;
    }


    protected function formatGateways(array $gateways)
    {
        $formatted = [];
        $config = $this->sms->getConfig();
        foreach ($gateways as $gateway => $setting) {
            if (is_int($gateway) && is_string($setting)) {
                $gateway = $setting;
                $setting = [];
            }
            $formatted[$gateway] = $setting;
            $globalSetting = $config->get("gateways.{$gateway}", []);
            if (is_string($gateway) && !empty($globalSetting) && is_array($setting)) {
                $formatted[$gateway] = array_merge($globalSetting, $setting);
            }
        }
        return $formatted;
    }
}