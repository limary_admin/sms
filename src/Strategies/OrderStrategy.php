<?php

namespace Sinta\Sms\Strategies;


use Sinta\Sms\Contracts\StrategyInterface;

class OrderStrategy implements StrategyInterface
{
    public function apply(array $gateways)
    {
        return array_keys($gateways);
    }
}