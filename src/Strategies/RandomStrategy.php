<?php

namespace Sinta\Sms\Strategies;


use Sinta\Sms\Contracts\StrategyInterface;

class RandomStrategy implements StrategyInterface
{
    public function apply(array $gateways)
    {
        uasort($gateways, function () {
            return mt_rand() - mt_rand();
        });
        return array_keys($gateways);
    }
}