<?php

namespace Sinta\Sms\Exceptions;

use Throwable;

/**
 * 没有可能网关异常
 *
 * Class NoGatewayAvailableException
 * @package Sinta\Sms\Exceptions
 */
class NoGatewayAvailableException extends Exception
{
    public $results = [];


    public function __construct(array $results = [], $code = 0, Throwable $previous = null)
    {
        $this->results = $results;
        parent::__construct('All the gateways have failed.', $code, $previous);
    }
}