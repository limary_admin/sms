<?php

namespace Sinta\Sms\Exceptions;

/**
 * 网关异常
 *
 * Class GatewayErrorException
 * @package Sinta\Sms\Exceptions
 */
class GatewayErrorException extends Exception
{

    public $raw = [];


    public function __construct($message, $code, array $raw = [])
    {
        parent::__construct($message, intval($code));
        $this->raw = $raw;
    }
}