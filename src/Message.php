<?php

namespace Sinta\Sms;

use Sinta\Sms\Contracts\GatewayInterface;
use Sinta\Sms\Contracts\MessageInterface;

/**
 * 消息类实现
 *
 * Class Message
 * @package Sinta\Sms
 */
class Message implements MessageInterface
{
    protected $gateways = [];

    /**
     * 消息类型
     *
     * @var string
     */
    protected $type;

    /**
     * 消息内容
     *
     * @var string
     */
    protected $content;

    /**
     * 模板
     *
     * @var string
     */
    protected $template;


    protected $data = [];


    public function __construct(array $attributes = [], $type = MessageInterface::MESSAGE_TEXT)
    {
        $this->type = $type;

        foreach ($attributes as $property => $value) {
            if (property_exists($this, $property)) {
                $this->$property = $value;
            }
        }

    }

    /**
     * 消息类型
     *
     * @return string
     */
    public function getMessageType()
    {
        return $this->type;
    }


    public function setType(string $type)
    {
        $this->type = $type;
        return $this;
    }



    /**
     * 消息内容
     *
     * @param GatewayInterface|null $gateway
     * @return string
     */
    public function getContent(GatewayInterface $gateway = null)
    {
        return $this->content;
    }


    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }


    /**
     * 模板
     *
     * @param GatewayInterface|null $gateway
     * @return mixed
     */
    public function getTemplate(GatewayInterface $gateway = null)
    {
        return $this->template;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }


    public function getData(GatewayInterface $gateway = null)
    {
        return $this->data;
    }

    public function setData(array $data)
    {
        $this->data = $data;
        return $this;
    }


    public function getGateways()
    {
        return $this->gateways;
    }


    public function setGateways(array $gateways)
    {
        $this->gateways = $gateways;
        return $this;
    }

    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

}