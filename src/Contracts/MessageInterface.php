<?php

namespace Sinta\Sms\Contracts;

/**
 * 消息接口
 *
 * Interface MessageInterface
 * @package Sinta\Sms\Contracts
 */
interface MessageInterface
{
    const MESSAGE_TEXT = 'text';
    const MESSAGE_VOICE = 'voice';

    /**
     *消息类型
     *
     * @return string
     */
    public function getMessageType();


    /**
     * 消息内容
     *
     * @param GatewayInterface|null $gateway
     * @return string
     */
    public function getContent(GatewayInterface $gateway = null);

    /**
     * 模板
     *
     * @param GatewayInterface|null $gateway
     * @return string
     */
    public function getTemplate(GatewayInterface $gateway = null);

    /**
     * 获取模板数据
     *
     * @param GatewayInterface|null $gateway
     * @return mixed
     */
    public function getData(GatewayInterface $gateway = null);


    /**
     * 支持的网关
     *
     * @return array
     */
    public function getGateways();
}