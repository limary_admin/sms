<?php
namespace Sinta\Sms\Contracts;

/**
 * 策略接口
 *
 * Interface StrategyInterface
 * @package Sinta\Sms\Contracts
 */
interface StrategyInterface
{

    public function apply(array $gateways);
}