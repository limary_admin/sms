<?php
namespace Sinta\Sms\Contracts;
use Sinta\Sms\Support\Config;

/**
 * 网关接口
 *
 * Interface GatewayInterface
 * @package Sinta\Sms\Contracts
 */
interface GatewayInterface
{
    /**
     * 网关名字
     *
     * @return mixed
     */
    public function getName();


    /**
     * 发送消息
     *
     * @param $to
     * @param MessageInterface $message
     * @param Config $config
     * @return mixed
     */
    public function send($to, MessageInterface $message, Config $config);
}